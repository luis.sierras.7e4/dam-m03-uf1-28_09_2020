package cat.itb.luissierras7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class NextSecond {
    public static void main(String[] args) {
        /*L'usuari introdueix una hora amb tres enters (hores, minuts i segons).*/
        System.out.println("introduce hora, minutos y segundos, por separado");
        Scanner scanner = new Scanner(System.in);
        int hora = scanner.nextInt();
        int minuto = scanner.nextInt();
        int segundo = scanner.nextInt();

        segundo++;

        if (segundo>=60) {
            int segundo2 = segundo-60;
            int minuto2 =minuto+1;
            if (minuto2>=60){
                int minuto3 = minuto2-60;
                int hora2 =hora+1;
                if (hora2>=24){
                    int dia = 1;
                    int hora3 = hora2 - 24;
                    System.out.println("dia: "+dia+" hora: "+hora3+":"+minuto3+":"+segundo2);
                }else {
                    System.out.println(hora2 + ":" + minuto3 + ":" + segundo2);
                }
            }else{
                System.out.println(hora+":"+minuto2+":"+segundo2);
            }
        }else{
            System.out.println(hora +":"+minuto+":"+segundo);
            }
    }
}
