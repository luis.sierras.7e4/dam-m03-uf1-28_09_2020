package cat.itb.luissierras7e4.dam.m03.uf1.seleccio.exam;

import java.util.Scanner;


public class ThreeCardPoker {
    public static void main(String[] args) {
        //L'usuari introduïrà els números de les 3 cartes i li hem de dir quina combinació té.
        System.out.println("Numeros");
        Scanner scanner=new Scanner(System.in);
        int carta1=scanner.nextInt();
        int carta2=scanner.nextInt();
        int carta3=scanner.nextInt();

        if (carta1==carta2 && carta1==carta3){
            System.out.println("trio");
        }else if (carta1==carta2 || carta1==carta3){
            System.out.println("Parella");
        }else if (carta2==carta1+1 && carta3==carta2+1 || carta3==carta1+1 && carta2==carta3+1 ||carta1==carta2+1 && carta3==carta1+1 || carta1==carta3+1 && carta3==carta2+1 || carta2==carta3+1 && carta1==carta2+1 || carta2==carta1+1 && carta1==carta3+1){
            System.out.println("Escala");
        }else{
            System.out.println("Numero alt");
        }
    }
}
