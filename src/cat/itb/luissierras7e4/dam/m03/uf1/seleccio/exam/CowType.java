package cat.itb.luissierras7e4.dam.m03.uf1.seleccio.exam;

import java.util.Scanner;


public class CowType {
    public static void main(String[] args) {
        //Demana a l'usuari l'edat del animal, el sexe (1 = mascle, 2 = femella) i si està capat (1 = no capat, 2 = capat).
        System.out.println("introdueix edat, sexe i si està capat");
        Scanner scanner = new Scanner(System.in);
        double edat=scanner.nextInt();
        int sexe=scanner.nextInt();
        int capat=scanner.nextInt();

        //Un animal es considera vedell si és menor de 2 anys, en cas contrari és adult.
        if (edat<=2){
            System.out.println("vadell");
        //Dels adults, es considera vaca si és femella i toro o bou si és mascle.
        }else if (edat>2 && sexe==2){
            System.out.println("vaca");
        //La diferència entre toro i bou és que el bou està capat i el toro no.
        }else if (edat>2 && capat==1){
            System.out.println("toro");
        }else if (edat>2 && capat==2){
            System.out.println("bou");
        }
    }
}
