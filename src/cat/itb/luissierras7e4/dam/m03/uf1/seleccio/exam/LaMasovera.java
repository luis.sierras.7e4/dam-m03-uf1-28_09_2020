package cat.itb.luissierras7e4.dam.m03.uf1.seleccio.exam;

import java.util.Scanner;


public class LaMasovera {
    public static void main(String[] args) {
        //L'usuari ha d'escriure el dia de la setmana en lletres (dilluns, dimarts, etc)
        System.out.println("Dia de la setmana");
        Scanner scanner =new Scanner(System.in);
        String dia=scanner.next();

        //Imprimeix per pantalla què ha de comprar.
        switch (dia){
            case "dilluns":
                System.out.println("Compra llums");
                break;
            case "dimarts":
                System.out.println("Compra naps");
                break;
            case "dimecres":
                System.out.println("Compra nespres");
                break;
            case "dijous":
                System.out.println("Compra nous");
                break;
            case "divendres":
                System.out.println("Compra tendres");
                break;
            case "dissabte":
                System.out.println("Tot s'ho gasta");
                break;
            case "diumenge":
                System.out.println("Tot s'ho menja");
                break;
        }
    }
}
