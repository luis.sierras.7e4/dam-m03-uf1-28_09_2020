package cat.itb.luissierras7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class NiceIsValidNote {
    public static void main(String[] args) {
        /*L'usuari escriu un enter*/
        Scanner entero = new Scanner(System.in);
        int valor = entero.nextInt();

        /*imprimeix bitllet vàlid si existeix un bitllet d'euros amb la quantitat entrada, bitllet invàlid en qualsevol altre cas.*/
        int billete5 = 5;
        int billete10 = 10;
        int billete20 = 20;
        int billete50 = 50;
        int billete100 = 100;
        int billete200 = 200;
        int billete500 = 500;

        boolean resultado = valor == billete5 || valor ==billete10 || valor ==billete20 || valor ==billete50 || valor ==billete100 || valor ==billete200 || valor ==billete500;
        if (resultado){
            System.out.println("bitllet vàlid");
        }else{
            System.out.println("bitllet invàlid");
        }
    }
}
