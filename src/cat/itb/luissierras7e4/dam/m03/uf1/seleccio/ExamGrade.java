package cat.itb.luissierras7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class ExamGrade {
    public static void main(String[] args) {
        /*L'usuari escriu un valor que representa una nota*/
        Scanner valor = new Scanner(System.in);
        System.out.println("nota");
        double nota = valor.nextDouble();

        /*Imprimeix "Excelent", "Notable", "Bé", "Suficient", "Suspès", "Nota invàlida" segons el la nota numèrica introduïda*/
        if (nota>=9 && nota<10){
            System.out.println("Excelent");
        } else if (nota>=7 && nota<9){
            System.out.println("Notable");
        } else if (nota>=6 && nota<7){
            System.out.println("Bé");
        } else if (nota>=5 && nota<6){
            System.out.println("Suficient");
        } else if (nota>=0 && nota<5){
            System.out.println("Suspès");
        } else {
            System.out.println("Nota invàlida");
        }
    }
}
