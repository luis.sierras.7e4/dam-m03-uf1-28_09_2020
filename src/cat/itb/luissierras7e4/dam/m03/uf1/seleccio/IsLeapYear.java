package cat.itb.luissierras7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class IsLeapYear {
    public static void main(String[] args) {
        /*L'usuari introdueix un any. Indica si és de traspàs printant "2020 és any de traspàs" o "2021 no és any de traspàs".*/
        Scanner dato =new Scanner(System.in);
        System.out.println("Introduce año");
        int year= dato.nextInt();

        /* calculo año bisiesto "especial"*/
        boolean bisiesto = year%100==0 && year%400==0;

        if (year%4==0||bisiesto){
            System.out.println(year+" és any de traspàs");
        } else {
            System.out.println(year+" no és any de traspàs");
        }
    }
}
