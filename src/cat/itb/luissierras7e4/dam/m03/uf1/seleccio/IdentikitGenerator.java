package cat.itb.luissierras7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class IdentikitGenerator {
    public static void main(String[] args) {
        //preguntas
        Scanner scanner=new Scanner(System.in);
        System.out.println("Com té els cabells: arrisats, llisos o pentinats.");
        String cabell= scanner.next();
        System.out.println("Com té els ulls: aclucats, rodons o estrellats.");
        String ulls= scanner.next();
        System.out.println("Com té el nas: aixafat, arromangat o aguilenc.");
        String nas= scanner.next();
        System.out.println("Com té la boca: normal, bigoti o dents sortides.");
        String boca= scanner.next();

        switch (cabell) {
            case "arrisats":
                System.out.println("@@@@@");
                break;
            case "llisos":
                System.out.println("VVVVV");
                break;
            case "pentinats":
                System.out.println("XXXXX");
                break;
            default:
                System.out.println("ERROR");
                break;
        }
        switch (ulls){
            case "aclucats":
                System.out.println(".-.-.");
                break;
            case "rodons":
                System.out.println(".o-o.");
                break;
            case "estrellats":
                System.out.println(".*-*.");
                break;
            default:
                System.out.println("ERROR");
        }
        switch (nas){
            case "aixafat":
                System.out.println("..0..");
                break;
            case "arromangat":
                System.out.println("..C..");
                break;
            case "aguilenc":
                System.out.println("..V..");
                break;
            default:
                System.out.println("ERROR");
        }
        switch (boca){
            case "normal":
                System.out.println(".===.");
                break;
            case "bigoti":
                System.out.println(".∼∼∼.");
                break;
            case "dents sortides":
                System.out.println(".www.");
                break;
            default:
                System.out.println("ERROR");
        }

    }
}
