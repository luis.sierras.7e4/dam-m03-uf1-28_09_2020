package cat.itb.luissierras7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class HowManyDaysInMonth {
    public static void main(String[] args) {
        //Demanar un enter a l'usuari que indica el numero de més
        System.out.println("Dame el mes");
        Scanner scanner= new Scanner(System.in);
        int mes= scanner.nextInt();

        //Retorna el número de dies del mes.
        //TODO switch
        if (mes<9)
            if (mes%2==0 && mes!=2 && mes!=8)
                System.out.println("este mes tiene 30 dias");
            else if (mes==2)
                System.out.println("este mes tiene 28 o 29 dias, asi es febrero");
            else
                System.out.println("este mes tiene 31 dias, asi es agosto");
        else if (mes%2==0)
            System.out.println("este mes tiene 31 dias");
        else
            System.out.println("este mes tiene 30 dias");
    }
}
