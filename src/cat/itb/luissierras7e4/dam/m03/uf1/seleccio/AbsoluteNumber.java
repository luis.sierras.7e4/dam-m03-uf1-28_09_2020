package cat.itb.luissierras7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class AbsoluteNumber {
    public static void main(String[] args) {
        /*Printa el valor absolut d'un enter entrat per l'usuari.*/
        Scanner valor = new Scanner(System.in);
        int entero= valor.nextInt();
        System.out.println("|"+entero+"|");
    }
}
