package cat.itb.luissierras7e4.dam.m03.uf1.strings.practice;


import java.util.Scanner;

public class RepositoryName {
// Implementar un programa que donat el número del mòdul, el número de la unitat formativa
// i el correu electrònic de l'ITB d'un alumne, ens calcule quin serà el nom del repositori de Git.

// El número del mòdul ha de tindre 2 xifres sempre, omplint amb 0's a l'esquerre si escau.

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introdueix el número del mòdul:");
        int modul = scanner.nextInt();

        System.out.println("El número de la UF:");
        int uf = scanner.nextInt();
        scanner.nextLine();

        System.out.println("El teu correu:");
        String mail = scanner.nextLine();
        String id = mail.split("@")[0];
        id = id.replace(".", "");

        System.out.println("Nom del teu repositori:");
        System.out.printf("%s-dam-m%02d-uf%d", id, modul, uf);

    }
}
