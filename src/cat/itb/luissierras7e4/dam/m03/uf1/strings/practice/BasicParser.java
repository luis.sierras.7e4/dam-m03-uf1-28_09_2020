package cat.itb.luissierras7e4.dam.m03.uf1.strings.practice;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BasicParser {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Text a analitzar sintàcticament (END per acabar):");
        String text = scanner.nextLine();
        List<String> newText = new ArrayList<String>();
        while (!text.equals("END")) {
            text = text.replace("**", "\\u001B[1m")
                    .replace("--", "\\u001B[3m")
                    .replace("++", "\\u001B[0m")
                    .replace("//r//", "\\u001B[31m")
                    .replace("//g//", "\\u001B[32m")
                    .replace("//b//", "\\u001B[34m")
                    .replace("//s//", "\\u001B[39m");
            newText.add(text);
            text = scanner.nextLine();
        }
        for (String s : newText) {
            System.out.println(s);
        }

    }
}
