package cat.itb.luissierras7e4.dam.m03.uf1.strings;

import java.util.Scanner;

public class UnaMoscaGenerator {
    public static void main(String[] args) {
//La canço tradicional de la mosca és la següent:
        //no canço
//Un cop cantada, la canço es canta substituint totes les vocals per una de sola (totes les vocals canviades per la A).
//Volem fer un programa que es digui la lletra donada la vocal amb la que volem substituir.

        Scanner scanner=new Scanner(System.in);
        char vocal= scanner.next().charAt(0);

        String canco="una mosca volava per la llum.\n" +
                "i la llum es va apagar.\n" +
                "i la pobra mosca\n" +
                "es va quedar a les fosques\n" +
                "i la pobra mosca no va poder volar.";

        System.out.print(canco.replace('a', vocal).replace('e', vocal).replace('i', vocal).replace('o', vocal).replace('u', vocal));
    }
}
