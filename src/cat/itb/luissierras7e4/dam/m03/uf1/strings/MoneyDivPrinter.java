package cat.itb.luissierras7e4.dam.m03.uf1.strings;

import java.text.DecimalFormat;
import java.util.Scanner;

public class MoneyDivPrinter {
    public static void main(String[] args) {
//L'usuari introdueix una quantitat de euros i un numero de persones a repartir. Imprimeix quans euros toquen a cada u amb el format següent:
//Si tenim 10.00€ i 3 persones, toquen a 3.33# per persona
        Scanner scanner=new Scanner(System.in);
        DecimalFormat dosDec = new DecimalFormat("#.00");

        float euros= scanner.nextInt();
        int personas=scanner.nextInt();
        float total=euros/personas;

        System.out.printf("Si tenim %s€ i %d persones, toquen a %s€ per persona",dosDec.format(euros),personas,dosDec.format(total));

    }
}
