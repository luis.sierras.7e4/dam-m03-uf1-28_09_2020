package cat.itb.luissierras7e4.dam.m03.uf1.strings;

import java.util.Scanner;

public class OneYesOneNo {
    public static void main(String[] args) {
//L'usuari introduirà una paraula. Imprimeix només les lletres en posició senar:
        Scanner scanner=new Scanner(System.in);
        String paraula= scanner.next();

        for (int i = 0; i < paraula.length(); i++) {
            if (i%2==0){
                System.out.print(paraula.charAt(i));
            }
        }
    }
}
