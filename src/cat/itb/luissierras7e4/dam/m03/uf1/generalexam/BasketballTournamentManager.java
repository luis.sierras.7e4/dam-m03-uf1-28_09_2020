package cat.itb.luissierras7e4.dam.m03.uf1.generalexam;

import java.sql.PreparedStatement;
import java.util.Scanner;

public class BasketballTournamentManager {

    public static void main(String[] args) {
//Una associació que organitza tornejos de basquet ens ha trucat per a que li montem un petit programa per a un campionat.
//El campionat consta de 8 equips que van fent partits entre ells.
//Volem que l'usuari pugui introduir l'equip que ha guanyat un partit i el programa ens avisi de quantes vitories porta, i al final, de qui és el guanyador.
        Scanner scanner=new Scanner(System.in);
        int equip= scanner.nextInt();
        int[] victorias=new int[8];
        int best=0;
        int resultado=0;
        while (equip!=-1){
            switch (equip){
                case 1:
                    victorias[0]++;
                    System.out.println("L'equip 1 té " +victorias[0]+" victòries");
                    break;
                case 2:
                    victorias[1]++;
                    System.out.println("L'equip 2 té "+victorias[1]+" victòries");
                    break;
                case 3:
                    victorias[2]++;
                    System.out.println("L'equip 3 té "+victorias[2]+" victòries");
                    break;
                case 4:
                    victorias[3]++;
                    System.out.println("L'equip 4 té "+victorias[3]+" victòries");
                    break;
                case 5:
                    victorias[4]++;
                    System.out.println("L'equip 5 té "+victorias[4]+" victòries");
                    break;
                case 6:
                    victorias[5]++;
                    System.out.println("L'equip 6 té "+victorias[5]+" victòries");
                    break;
                case 7:
                    victorias[6]++;
                    System.out.println("L'equip 7 té "+victorias[6]+" victòries");
                    break;
                case 8:
                    victorias[7]++;
                    System.out.println("L'equip 8 té "+victorias[7]+" victòries");
                    break;
            }
            equip= scanner.nextInt();
        }
        for (int i = 0; i < victorias.length; i++) {
            if(victorias[i]>best){
                resultado =i+1;
                best=victorias[i];
            }
        }
        System.out.println("L'equip guanyador és el "+resultado);
        //no se com fer el guanyador :), al final ho he aconseguit :)))
    }
}
