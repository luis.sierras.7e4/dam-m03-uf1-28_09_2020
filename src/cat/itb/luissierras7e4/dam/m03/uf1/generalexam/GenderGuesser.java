package cat.itb.luissierras7e4.dam.m03.uf1.generalexam;

import java.util.Scanner;

public class GenderGuesser {
    public static void main(String[] args) {
//Treballem en una empresa que és dedica al software d'analisis de text.
//Ens han encomanat la tasca de fer un petit programa que donada una paraula ens indiqui si és masculina, femenina o plural.
//L'algoritme és complicat però el simplificarem de la següent manera. Tota paraula que acabi en a serà consideradad feminina, mentre que si acaba en s serà plural.
//La resta masculines.

        Scanner scanner=new Scanner(System.in);
        String paraula=scanner.next();
        String S="s";
        String A="a";
        char s = S.charAt(0);
        char a = A.charAt(0);
        while (!paraula.equals("END")) {
            char ultim = paraula.charAt(paraula.length() - 1);
            if (ultim == a) {
                System.out.println("femení");
            } else if (ultim == s){
                System.out.println("plural");
            } else{
                System.out.println("masculí");
            }

            paraula=scanner.next();
        }
    }
}
