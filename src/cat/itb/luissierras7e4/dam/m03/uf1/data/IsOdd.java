package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsOdd {
    public static void main(String[] args) {
        /* pedir entero */
        Scanner scanner = new Scanner(System.in);
        int entero = scanner.nextInt();

        /* comprobar si es impar */
        boolean impar = entero % 2 !=0;
        System.out.println(impar);
    }
}
