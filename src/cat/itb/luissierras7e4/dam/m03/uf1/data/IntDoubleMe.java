package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IntDoubleMe {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int userInputValue = scanner.nextInt();
        int total = userInputValue * 2 ;
        System.out.println(total);

    }
}
