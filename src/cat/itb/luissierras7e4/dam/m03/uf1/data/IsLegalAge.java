package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsLegalAge {
/*L'usuari escriu un enter amb la seva edat i s'imprimeix true si és major d'edat, i false en qualsevol altre cas.*/
    public static void main(String[] args) {

        /* ask age */
        Scanner scanner = new Scanner(System.in);
        int age = scanner.nextInt();

        /* check age is legal (age >=18)*/
        boolean isLegalAge= age >= 18;

        /* print true/false */
        System.out.println(isLegalAge);

    }
}
