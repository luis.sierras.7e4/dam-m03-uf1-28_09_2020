package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class DivideBill {
    public static void main(String[] args) {
        /*L'usuari introdueix el número de començals i el preu d'un sopar en cèntims d'euro.*/
        Scanner scan = new Scanner(System.in);
        System.out.println("Començals");
        int comencals= scan.nextInt();
        System.out.println("preu en cts");
        int preu=scan.nextInt();

        /*Imprimeix quan haurà de pagar cada començal.*/
        double resultado= (preu / comencals);
        System.out.println("han de pagar "+resultado+" cts cadascun.");
    }
}
