package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class BiggerPizza {
    public static void main(String[] args) {
        /*Volem comparar quina pizza és més gran, entre una rectangular i una rodona*/
        /*L'usuai entra el diametre d'una pizza rodona*/
        Scanner valores=new Scanner(System.in);
        System.out.println("esee diametro");
        double diametro= valores.nextDouble();
        double radio= diametro/2;
        double pi= 3.14159265359;
        double area= Math.pow(radio,2)*pi;

        /*L'usuari entra els dos costats de la pizza rectangular*/
        System.out.println("esa base y altura loko");
        double base= valores.nextDouble();
        double altura= valores.nextDouble();
        double area2= base * altura;

        /*Imprimeix true si la pizza rodona és més gran, o false en qualsevol altre cas.*/
        boolean resultado= area>area2;
        System.out.println(resultado);
    }
}
