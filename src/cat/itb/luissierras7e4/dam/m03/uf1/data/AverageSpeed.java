package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class AverageSpeed {
    public static void main(String[] args) {
        /*L'usuari introdueix els quilometres recorreguts i el temps minuts que ha tardat en fer el recorregut*/
        Scanner scanner = new Scanner(System.in);
        System.out.println("km recorridos: ");
        int km = scanner.nextInt();
        System.out.println("tiempo en minutos: ");
        int min = scanner.nextInt();

        /*calculos chupi guai*/
        double hora = min / 60;
        double velocidad = km / hora;

        /*Printa per pantalla la velocitat mitjana en km/hora*/
        System.out.println("la velocidad media es de "+velocidad +"Km/Hora");
    }
}
