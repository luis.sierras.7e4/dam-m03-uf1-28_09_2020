package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class DoubleDoubleMe {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double valor = scanner.nextDouble();
        System.out.println(valor * 2.0);
    }
}
