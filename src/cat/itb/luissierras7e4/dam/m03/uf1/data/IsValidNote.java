package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsValidNote {
/* L'usuari escriu un enter i s'imprimeix true si existeix un bitllet d'euros amb la quantitat entrada, false en qualsevol altre cas. */
    public static void main(String[] args) {
        /*solicitar valor entero */
        Scanner scanner = new Scanner(System.in);
        int Valor = scanner.nextInt();

        /* comprobar si exixte billete euro */
        boolean billete5 = Valor == 5;
        boolean billete10 = Valor == 10;
        boolean billete20 = Valor == 20;
        boolean billete50 = Valor == 50;
        boolean billete100 = Valor == 100;
        boolean billete200 = Valor == 200;
        boolean billete500 = Valor == 500;

        boolean billete = billete5 || billete10 || billete20 || billete50 || billete100 || billete200 || billete500 ;

        /* imprimir resultado */
        System.out.println(billete);

    }
}
