package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsGreater {
/*L'usuari escriu dos valors i s'imprimeix true si el primer és més gran que el segon i false en qualsevol altre cas.*/
    public static void main(String[] args) {
    /* solicitar 2 valores */
        Scanner scanner = new Scanner(System.in);
        int valor1 = scanner.nextInt();
        int valor2 = scanner.nextInt();
    /* comprobar primer valor mayor al segundo */
        boolean mayor = valor1 > valor2;
    /* imprimir resultado */
        System.out.println(mayor);
    }
}
