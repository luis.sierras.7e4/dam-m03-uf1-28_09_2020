package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IntSum {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int first = scanner.nextInt();
        int second = scanner.nextInt();
        System.out.println(first + second);
    }

}
