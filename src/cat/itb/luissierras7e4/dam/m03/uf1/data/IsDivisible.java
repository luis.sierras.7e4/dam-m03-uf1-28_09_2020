package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsDivisible {
    public static void main(String[] args) {
        /* L'usuari escriu dos valors enters.*/
        Scanner enteros =new Scanner(System.in);
        int primero= enteros.nextInt();
        int segundo= enteros.nextInt();

        /*Imprimeix si el primer és divisible pel segon (si el mòdul de dividir el primer amb el segon és 0).*/
        int modul = primero % segundo;
        boolean resultado = modul == 0;
        System.out.println(resultado);
    }
}