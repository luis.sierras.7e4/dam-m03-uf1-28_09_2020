package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArraySortedValues {
    public static void main(String[] args) {
        //L'usuari primer entrarà el número d'enters a introudir i després els diferents enters.

        /*MODIFICADO
        int total= scanner.nextInt();
        for(int i=0; i<total; ++i){
            enters[i] = scanner.nextInt();
        }*/
        Scanner scanner = new Scanner(System.in);
        int[] enters = ArrayReader.scannerReadIntArray(scanner);

        //Printa per pantalla ordenats si la llista de N valors introduits per l'usuari estan ordenats.
        boolean orden= true;
        for(int i=0; i+1<enters.length; ++i){
            int valor = enters[i];
            int newValor= enters[i+1];
            if(newValor<valor){
                orden=false;
                break;
            }
        }
        if(orden){
            System.out.println("ordenat");
        }
    }
}
