package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class BoxesOpenedCounter {
    public static void main(String[] args) {
//Un banc té tot de caixes de seguretat, enumerades del 0 al 10.
//Volem registar quan els usuaris obren una caixa de seguretat, i al final del dia, fer-ne un recompte.
    //L'usuari introduirà enters del 0 al 10 quan s'obri la caixa indicada.
    //Quan introduiexi l'enter -1, és que s'ha acabat el dia. Printa per pantalla el nombre de cops que s'ha obert.
        int[] caixes=new int[11];
        Scanner scanner=new Scanner(System.in);
        int mirar = scanner.nextInt();
        while (mirar!=-1){
            caixes[mirar]+=1;
            mirar = scanner.nextInt();
        }
        System.out.println(Arrays.toString(caixes));
    }
}
