package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class PushButtonPadlockSimulator {
    public static void main(String[] args) {
        //Volem fer un simulador d'un candau com el de la foto:
        //L'usuari introduirà enters indicant quin botó ha d'apretar (o desapretar)
        //Quan introdueixi el -1, és que ja ha acabat i hem d'imprimir l'estat del candau
        Scanner scanner=new Scanner(System.in);
        int boton = scanner.nextInt();
        boolean[] candado=new boolean[8];
        while(boton!=-1){
            candado[boton]= !candado[boton];
            boton= scanner.nextInt();
        }
        System.out.println(Arrays.toString(candado));
    }
}
