package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.text.DecimalFormat;
import java.util.Scanner;

public class IvaPrices {
    public static void main(String[] args) {
        //En una botiga volem convertir tot de preus sense a IVA al preu amb IVA. Per afegir l'IVA a un preu hem de sumar-hi el 21% del seu valor.
        //L'usuari introduirà el preu de 10 artícles. Imprimeix per pantalla el preu amb l'IVA afegit amb el següent format:
        //8.0 IVA = 9.68
        //4.0 IVA = 4.84

        DecimalFormat dosDec = new DecimalFormat("#.00");
        Scanner scanner = new Scanner(System.in);
        double[] preu= new double[10];
        for (int i=0; i< preu.length; ++i){
           double producte = scanner.nextDouble();
           preu[i]=producte*1.21;
        }
        for (double valor : preu) {
            System.out.println((valor / 1.21) + " IVA = " + dosDec.format(valor));
        }
    }
}
