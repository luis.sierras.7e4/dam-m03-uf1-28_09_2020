package cat.itb.luissierras7e4.dam.m03.uf1.arrays;


import java.util.Scanner;

public class SimpleBattleshipResult {
    public static void main(String[] args) {
        //Donada la següent configuració del joc Enfonsar la flota, indica si a la posició x, y hi ha aigua o un vaixell (tocat)
        //On 1 són baixells i els 0 aigua, casella superior esquerra és la 0,0 i la superior dreta la 0,6
        // 1 1 0 0 0 0 1
        // 0 0 1 0 0 0 1
        // 0 0 0 0 0 0 1
        // 0 1 1 1 0 0 1
        // 0 0 0 0 1 0 0
        // 0 0 0 0 1 0 0
        // 1 0 0 0 0 0 0

        Scanner scanner = new Scanner(System.in);
        boolean[][] flota=
                {{true,true,false,false,false,false,true},
                {false,false,true,false,false,false,true},
                {false,false,false,false,false,false,true},
                {false,true,true,true,false,false,true},
                {false,false,false,false,true,false,false},
                {false,false,false,false,true,false,false},
                {true,false,false,false,false,false,false}};

        System.out.println("indica coordenadas X Y");
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        if (flota[x][y]){
            System.out.println("tocat");
        } else{
            System.out.println("aigua");
        }
    }
}
