package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class AddValuesToArray {
    public static void main(String[] args) {
//Inicialitza un array de floats de tamany 50, amb el valor 0.0f a tots els elements.
        float[] valors = new float[50];
        valors[0] = 31;
        valors[1] = 56;
        valors[19] = 12;
        valors[49] = 79;
       System.out.println(Arrays.toString(valors));
    }
}
/*      //PROGRAMA ORIGINAL
        Scanner scanner=new Scanner(System.in);
        int posicio;
        while(posicio!=49) {
            float numero = scanner.nextFloat();
            posicio = scanner.nextInt();
            valors[posicio] = numero;
        }
*/
