package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class MatrixBoxesOpenedCounter {
    public static void main(String[] args) {
        //Un banc té tot de caixes de seguretat en una graella, enumerades per fila i columna del 0 al 3.
        //Volem registar quan els usuaris obren una caixa de seguretat, i al final del dia, fer-ne un recompte.
        //
        //    L'usuari introduirà parells d'entrers del 0 al 3 quan s'obri la caixa indicada.
        //    Quan introduiexi l'enter -1, és que s'ha acabat el dia. Printa per pantalla el nombre de cops que s'ha obert.
        int[][] caixes=new int[4][4];
        Scanner scanner=new Scanner(System.in);
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        while (y!=-1||x!=-1){
            caixes[x][y]+=1;
            x = scanner.nextInt();
            if(x==-1){
                break;
            }
            y = scanner.nextInt();
        }
        System.out.println(Arrays.deepToString(caixes));
    }
}

