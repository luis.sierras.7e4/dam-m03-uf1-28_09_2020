package cat.itb.luissierras7e4.dam.m03.uf1.arrays.exam;

import java.util.Arrays;
import java.util.Scanner;

public class ChessRockMatrix {
    public static void main(String[] args) {
//Un tauler d'escacs és una graella de 8x8 caselles. Una torre es pot moure en línia recta de forma vertical o horitzontal.
//
//Donada una posició d'una torre en un tauler d'escacs, fes una matriu de booleans indicant a quins punts es pot moure la torre en el següent torn.
        Scanner scanner=new Scanner(System.in);
        boolean[][] graella=new boolean[8][8];
        int posicio=scanner.nextInt();
//TODO CORREGIR
        for (int i = 0; i < graella.length; i++) {
            graella[posicio][i]=!graella[posicio][i];
            for (int j = 0; j <graella.length; j++) {
                graella[j][posicio]=!graella[j][i];
           }
        }
        System.out.println(Arrays.deepToString(graella));
    }
}
