package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArraySumValues {
    //L'usuari introduix una llista de valors tal i com s'indica al mètode ArrayReader.
    //Imprimeix per pantalla la suma d'aquests valors.
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int suma = 0;
        int[] enters = ArrayReader.scannerReadIntArray(scanner);
        for (int value:enters){
            suma += value;
        }
        System.out.println(suma);
    }

}
