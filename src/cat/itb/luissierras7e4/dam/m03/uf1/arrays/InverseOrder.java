package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class InverseOrder {
    public static void main(String[] args) {
        //L'usuari entra 10 enters. Imprimeix-los en l'odre invers al que els ha entrat.
        Scanner scanner = new Scanner(System.in);
        int[] enters = new int[10];
        for(int i=0;i<10;++i){
            enters[i] = scanner.nextInt();
        }
        for(int i=9;i>=0;--i){
            int value =enters[i];
            System.out.println(value);
        }
    }
}
