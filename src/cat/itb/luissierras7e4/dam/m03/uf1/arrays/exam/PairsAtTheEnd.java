package cat.itb.luissierras7e4.dam.m03.uf1.arrays.exam;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PairsAtTheEnd {
    public static void main(String[] args) {
//L'usuari introduirà enters. Afegeix els enters al inici de la llista si són parells, i al final si són senars. Quan entri el -1 s'acaba el programa i s'imprimeix la llista.
        Scanner scanner=new Scanner(System.in);
        List<Integer> enters = new ArrayList<>();
        int valor=scanner.nextInt();
//TODO ARREGLAR
        while (valor!=-1){
            if (valor%2==0){
                enters.add(0, valor);
            }else{
                enters.add(enters.size(), valor);
            }
            valor=scanner.nextInt();
        }
        System.out.println(enters);
    }
}
