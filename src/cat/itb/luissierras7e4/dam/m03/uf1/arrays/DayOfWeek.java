package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class DayOfWeek {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //Donat un enter, printa el dia de la setmana amb text (dilluns, dimarts, dimecres), tenint en compte que dilluns és el 0.
        String[] dias= {"dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte", "diumenge"};
        System.out.println("Enters de 0 a 6");
        int dia = scanner.nextInt();
        String valor = dias[dia];
        System.out.println(valor);
    }
}
