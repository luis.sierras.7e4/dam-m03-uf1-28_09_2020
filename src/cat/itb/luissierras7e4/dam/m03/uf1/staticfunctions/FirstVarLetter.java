package cat.itb.luissierras7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class FirstVarLetter {
    public static void main(String[] args) {
        //L'usuari escriu la primer caràcter de l'identificador d'una variable.
        System.out.println("introduce letra");
        Scanner scanner=new Scanner(System.in);
        char letter= scanner.next().charAt(0);

        //comprobar letra
        boolean esUnaLletra = Character.isLetter(letter);
        boolean lowerCase = Character.isLowerCase(letter);

        if (esUnaLletra && lowerCase){
            System.out.println("Caràcter vàlid");
        }else if (!lowerCase){
            System.out.println("Caràcter invàlid");
            System.out.println("'"+Character.toLowerCase(letter)+"'"+" es el correcte");
        }

    }
}