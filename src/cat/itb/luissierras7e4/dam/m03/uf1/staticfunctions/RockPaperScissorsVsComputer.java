package cat.itb.luissierras7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class RockPaperScissorsVsComputer {
    public static void main(String[] args) {
        /*L'usuari introdueix un enter (1)pedra, (2) paper, (3) tisora.
            L'ordinador decideix aleatoriament la seva tirada.*/
        System.out.println("introduce numero: (1)pedra, (2) paper, (3) tisora");
        Scanner scanner = new Scanner(System.in);
        int player= scanner.nextInt();
        int cpu = (int) (Math.random()*3 +1);

        //Imprimeix per pantalla que tira l'orinador (L'ordinador a tirat pedra).
        if (cpu==1) {
            System.out.println("L'ordinador ha tirat pedra");
        } else if (cpu==2){
            System.out.println("L'ordinador ha tirat paper");
        } else if (cpu==3){
            System.out.println("L'ordinador ha tirat tisora");
        }

        //Imprimeix per pantalla Has guanyat, Guanya l'ordinador, O empat segons el resultat.
        boolean empate = player == cpu;

        if (empate){
            goToTie();
        } else if (player==1 && cpu==3 || player==2 && cpu==1 || player==3 && cpu==2){
            player_win();
        }else if (cpu==1 && player==3 || cpu==2 && player==1 || cpu==3 && player==2){
            cpu_win();
        } else {
            System.out.println("ERROR");
        }
    }

    private static void cpu_win() {
        System.out.println("Has guanyat");
    }

    private static void player_win() {
        System.out.println("Guanya l'ordinador");
    }

    private static void goToTie() {
        System.out.println("empat");
    }

}

