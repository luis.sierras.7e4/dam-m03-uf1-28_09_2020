package cat.itb.luissierras7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class HowBigIsMyPizzaFunc {
    public static void main(String[] args) {
        System.out.println("Introduce Diametro Pizza");
        Scanner scanner = new Scanner(System.in);
        double diametro = scanner.nextDouble();

        double resultado= areaPizzaConDiametro(diametro);
        System.out.println(resultado);
    }

    public static double areaPizzaConDiametro(double diametro) {
        double radio=diametro/2;
        return Math.PI * Math.pow(radio,2);
    }

}
