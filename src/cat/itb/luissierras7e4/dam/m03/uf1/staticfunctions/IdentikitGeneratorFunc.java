package cat.itb.luissierras7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class IdentikitGeneratorFunc {
    public static void main(String[] args) {
        //preguntas
        Scanner scanner=new Scanner(System.in);
        System.out.println("Com té els cabells: arrisats, llisos o pentinats.");
        String cabellText= scanner.next();
        System.out.println("Com té els ulls: aclucats, rodons o estrellats.");
        String ullsText= scanner.next();
        System.out.println("Com té el nas: aixafat, arromangat o aguilenc.");
        String nasText= scanner.next();
        System.out.println("Com té la boca: normal, bigoti o dents-sortides.");
        String bocaText= scanner.next();

        String cabell = dibuixCabell(cabellText);
        String ulls = dibuixUlls(ullsText);
        String nas = dibuixNas(nasText);
        String boca = dibuixboca(bocaText);

        System.out.println(cabell);
        System.out.println(ulls);
        System.out.println(nas);
        System.out.println(boca);
    }

    private static String dibuixboca(String bocaText) {
        switch (bocaText) {
            case "normal":
                return ".===.";
            case "bigoti":
                return ".∼∼∼.";
            case "dents-sortides":
                return ".www.";
            default:
                return "error";
        }
    }

    private static String dibuixNas(String nasText) {
        switch (nasText) {
        case "aixafat":
            return "..0..";
        case "arromangat":
            return "..C..";
        case "aguilenc":
            return "..V..";
        default:
            return "error";
    }

    }

    private static String dibuixUlls(String ullsText) {
        switch (ullsText) {
            case "aclucats":
                return ".-.-.";
            case "rodons":
                return ".o-o.";
            case "estrellats":
                return ".*-*.";
            default:
                return "error";
        }
    }

    private static String dibuixCabell(String cabellText) {
        switch (cabellText) {
            case "arrisats":
                return "@@@@@";
            case "llisos":
                return "VVVVV";
            case "pentinats":
                return "XXXXX";
            default:
                return "error";
        }
    }
}