package cat.itb.luissierras7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class CountDown {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numero=scanner.nextInt();
        int i = numero;
        while (i>0) {
            System.out.print(i);
            i--;
        }
    }
}
