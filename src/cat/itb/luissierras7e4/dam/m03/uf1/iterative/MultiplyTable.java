package cat.itb.luissierras7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class MultiplyTable {
    public static void main(String[] args) {
        //L'usuari introdueix un enter
        Scanner scanner=new Scanner(System.in);
        int multiple = scanner.nextInt();
        int i = 1;
        //Printa per pantalla la taula de multiplicar del número introduit:
        while(i<=10){
            System.out.println(i +" * "+ multiple +" = "+ i*multiple);
            i++;
        }
    }
}
