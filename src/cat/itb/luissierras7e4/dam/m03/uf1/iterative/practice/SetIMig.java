package cat.itb.luissierras7e4.dam.m03.uf1.iterative.practice;

import java.util.Scanner;

public class SetIMig {
    public static void main(String[] args) {
        playUserTurn();
    }

    /**
     * Given the number of a card, it returns the value in a 7iMig game. 8, 9 and 10 represents jack, horse,
     * king respectively.
     * @param card between 1 and 10
     * @return value of the gien card.
     */
    public static double getCardValue(int card){
        if(card>=1 && card<=7) {
            return card;
        }else{
            return 0.5;
        }
    }

    /** Given the number of a card, it returns the name of the card. 8, 9 and 10 represents jack (sota), horse (cavall),
     king (rei) respectively.
     @param card between 1 and 10
     @return name of the given card. */

    public static String getCardText(int card) {
        switch (card) {
            case 8: return "sota";
            case 9: return "cavall";
            case 10: return "rei";
            default: return card + "";
        }
    }

    /**
     * Plays the computer turn.
     * @param userCounter user turn result. The computer will try to obtain at least the same value.
     */
    public static void playComputerTurn(double userCounter){
        double cpuCounter=0;
        while(cpuCounter<7.5 && cpuCounter<userCounter){
            int card = ItbRandomizer.nextInt(1,10);
            cpuCounter += getCardValue(card);
            System.out.println("L'ordinador ha tret un:" +getCardValue(card));
        }
        if (cpuCounter>7.5){
            System.out.println("L'ordinador s'ha pasat");
            System.out.println("Has guanyat!!!");
        }else if(cpuCounter>userCounter){
            System.out.println("El resultat de l'ordinador és: "+cpuCounter);
            System.out.println("Guanya la banca");
        }
    }

    /**
     * Plays the user turn.
     *
     */
    public static void playUserTurn(){
        Scanner scanner=new Scanner(System.in);
        double userCounter=0;
        int seguir = 1;
        while(seguir!=0) {
            int card = ItbRandomizer.nextInt(1, 10);
            System.out.println("T'ha sortit la carta:" +getCardText(card));
            userCounter += getCardValue(card);
            if (userCounter <= 7.5) {
                System.out.println("Resultat actual: "+userCounter);
                System.out.println("Vols seguir jugant?");
                seguir = scanner.nextInt();
            }else{
                System.out.println("T'has passat");
                System.out.println("Resultat actual: "+userCounter);
                System.exit(1);
            }
        }
        System.out.println("El teu resultat és: "+userCounter);
        playComputerTurn(userCounter);
    }
}
