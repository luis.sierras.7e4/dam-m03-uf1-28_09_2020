package cat.itb.luissierras7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class HowManyLines {
    public static void main(String[] args) {
        //L'usuari introduiex un text per pantalla que pot tenir salts de línia.
        Scanner scanner=new Scanner(System.in);
        String linea = scanner.nextLine();
        int i=0;

        //Per indicar que ha acabat d'introduïr el text escriurà una línia amb la paraula clau END
        while (!linea.equals("END")){
            i++;
            linea = scanner.nextLine();
        }

        //Imprimeix el nombre de línies que ha introduït l'usuari (sense contar la END)
        System.out.println(i);
    }
}
