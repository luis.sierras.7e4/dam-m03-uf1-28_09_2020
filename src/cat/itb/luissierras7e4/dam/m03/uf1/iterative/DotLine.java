package cat.itb.luissierras7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class DotLine {
    public static void main(String[] args) {
        //Demana un enter a l'usuari.
        Scanner scanner = new Scanner(System.in);
        int numero= scanner.nextInt();

        //Imprimeix per pantalla tants punts com l'usuari hagi indicat
        for(int i = 0; numero != i; i++){
            System.out.print(".");
        }
    }
}
