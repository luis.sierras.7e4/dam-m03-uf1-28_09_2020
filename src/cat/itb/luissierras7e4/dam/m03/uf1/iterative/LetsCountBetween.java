package cat.itb.luissierras7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class LetsCountBetween {
    public static void main(String[] args) {
        //L'usuari introdueix dos valors enters.
        Scanner scanner=new Scanner(System.in);
        int valor1= scanner.nextInt();
        int valor2= scanner.nextInt();
        int i=valor1;
        //Printa per pantalla tots els valors que hi ha entre els dos valors introduits ordenats de menor a major
        while (i<valor2-1){
            i++;
            System.out.print(i);
        }
    }
}
