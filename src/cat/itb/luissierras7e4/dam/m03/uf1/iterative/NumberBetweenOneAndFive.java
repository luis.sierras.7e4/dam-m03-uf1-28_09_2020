package cat.itb.luissierras7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class NumberBetweenOneAndFive {
    public static void main(String[] args) {
        //Demanar a l'usuari un enter entre 1 i 5.
        Scanner scanner = new Scanner(System.in);
        int valor;

        //Si introdueix un número més gran o més petit, torna-li a demanar l'enter.
        do{
            valor= scanner.nextInt();
        } while(valor<1 || valor>5);

        //Imprimeix per pantalla: El número introduït: 3, substituint el 3 pel número.
        System.out.println("El número introduït: "+valor);
    }
}
