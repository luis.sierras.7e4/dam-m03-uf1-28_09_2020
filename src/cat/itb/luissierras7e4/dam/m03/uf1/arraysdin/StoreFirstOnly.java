package cat.itb.luissierras7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StoreFirstOnly {
    public static void main(String[] args) {
//L'usuari introduirà enters. Enmagatzema en una llista la primera vegada que l'usuari entri un valor.
//Quan introdueixi un -1 és que ja ha acabat.

        Scanner scanner = new Scanner(System.in);
        int valor = scanner.nextInt();
        List<Integer> valores = new ArrayList<Integer>();
        valores.add(valor);

        while (valor != -1) {
            boolean esta = false;
            for (int v : valores) {
                if (valor == v) {
                    esta = true;
                    break;
                }
            }
            if (!esta) {
                valores.add(valor);
            }
            valor = scanner.nextInt();
        }
        System.out.println(valores);
    }
}
