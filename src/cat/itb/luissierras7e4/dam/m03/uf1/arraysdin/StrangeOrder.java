package cat.itb.luissierras7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StrangeOrder {
    public static void main(String[] args) {
//L'usuari entrarà un conjunt d'enters per pantalla. Quan introdueixi el -1, és que ja ha acabat. Afegeix el primer a l'inici de la llista, el segon al final, el tercer a l'inici, etc.
//Printa el resultat:

        Scanner scanner = new Scanner(System.in);
        int valor=scanner.nextInt();
        List<Integer> enters=new ArrayList<Integer>();

        while(valor!=-1) {
            enters.add(0, valor);
            valor=scanner.nextInt();
            if (valor==-1){
                break;
            }
            enters.add(enters.size(), valor);
            valor=scanner.nextInt();
        }
            System.out.println(enters);
    }
}
