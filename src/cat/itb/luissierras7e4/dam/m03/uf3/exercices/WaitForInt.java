package cat.itb.luissierras7e4.dam.m03.uf3.exercices;

import java.util.InputMismatchException;
import java.util.Scanner;

public class WaitForInt {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        scanInt(scanner);

    }


    private static void scanInt(Scanner scanner) {
        try{
            int value = scanner.nextInt();
            System.out.println("Número: "+value);
        } catch(InputMismatchException inputMismatchException){
            System.out.println("No és un enter");
        }
    }
}
