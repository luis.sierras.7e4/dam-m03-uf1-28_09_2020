package cat.itb.luissierras7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class GenerateDummyFileStructure {
    public static void main(String[] args) throws IOException {
        String homePath = System.getProperty("user.home");
        Path dummy = Files.createDirectories(Path.of(homePath,"dummyfolder"));

        for (int i=0; i<100; i++) {
            Path newDestination = dummy.resolve(i+" ");
            Files.createDirectory(newDestination);
        }
    }
}
