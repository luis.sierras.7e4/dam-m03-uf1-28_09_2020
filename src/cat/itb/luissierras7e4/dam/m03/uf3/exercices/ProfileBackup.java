package cat.itb.luissierras7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

public class ProfileBackup {
    public static void main(String[] args) throws IOException {
        //dir home usuario
        String homePath = System.getProperty("user.home");
        //fecha y hora actual
        String date = LocalDateTime.now().toString();

        //obtener ruta .profile
        Path profile = Paths.get(homePath,".profile");
        System.out.println(profile);

        //crear carpeta backup con fecha
        Path backup=Path.of(homePath,"backup",date);
        Files.createDirectories(backup);
        Path desti = backup.resolve(".profile");

        //copiar
        Files.copy(profile, desti);

    }
}
