package cat.itb.luissierras7e4.dam.m03.uf3.exercices;


import cat.itb.luissierras7e4.dam.m03.uf2.classfun.Rectangle;
import cat.itb.luissierras7e4.dam.m03.uf2.classfun.RectangleSize;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;


public class RectangleSizesFile {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        List<Rectangle> rectangles = readFromFile(scanner);
        RectangleSize.printRectangles(rectangles);
        System.out.println("Dades enregistrades");
    }

    private static List<Rectangle> readFromFile(Scanner scanner) throws IOException {
        String stringPath = scanner.nextLine();
        Path path = Paths.get(stringPath);
        Scanner fileScanner = new Scanner(path);
        return RectangleSize.readRectangles(fileScanner);
    }
}

