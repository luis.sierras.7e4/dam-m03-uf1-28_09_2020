package cat.itb.luissierras7e4.dam.m03.uf3.exercices;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FindFile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String folderPath = scanner.nextLine();
        Path folder = Path.of(folderPath);
        String fitxer= scanner.nextLine();
        Path path = Paths.get(folderPath);
        try{
            Stream<Path> fileStream = Files.list(folder);
            List<Path> files = fileStream.collect(Collectors.toList());
            for (Path file : files){
                if (Files.exists(path)){
                    System.out.println();
                }
            }
        }catch (IOException e){
            System.out.println("Fitxer no trobat");
        }
    }
}

