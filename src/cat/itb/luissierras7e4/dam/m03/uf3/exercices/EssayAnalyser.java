package cat.itb.luissierras7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Volem fer un programa que, donat un fitxer de text ens indiqui quantes línies té i quantes paraules té.
 */

public class EssayAnalyser {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Path del fitxer a comprovar:");
        String userPath = scanner.nextLine();
        Path path = Paths.get(userPath);

        countLines(path);
        countWords(path);
    }

    private static void countWords(Path path) throws IOException {
        Scanner scanner = new Scanner(path);
        int countWords = 0;
        while (scanner.hasNext()){
            scanner.next();
            countWords++;
        }
        System.out.printf("Número de paraules: %d", countWords);
    }

    private static void countLines(Path path) throws IOException {
        Scanner scanner = new Scanner(path);
        int countLines = 0;
        while (scanner.hasNext()){
            String line = scanner.nextLine();
            countLines++;
        }
        System.out.printf("Número de línies: %d", countLines);
    }
}

