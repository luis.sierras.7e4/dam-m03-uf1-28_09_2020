package cat.itb.luissierras7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

public class IWasHere {
    public static void main(String[] args) throws IOException {
        //dir home y fecha
        String homePath = System.getProperty("user.home");
        String date = LocalDateTime.now().toString();

        Path path = Path.of(homePath,"i_was_here.txt");
        try (OutputStream outputStream = Files.newOutputStream(path, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
            PrintStream printStream = new PrintStream(outputStream, true);
            printStream.println("I Was Here: " +date);

            /* comprobar
            System.out.println(path);
            String read= Files.readString(path);
            System.out.println(read);
            */
        }

    }
}
