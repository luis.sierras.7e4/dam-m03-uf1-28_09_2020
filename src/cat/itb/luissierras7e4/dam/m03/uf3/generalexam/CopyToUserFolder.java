package cat.itb.luissierras7e4.dam.m03.uf3.generalexam;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;


public class CopyToUserFolder {
    public static void main(String[] args) throws IOException {


/*Volem fer un petit programa que fagi una copia del fitxer ~/.bashrc a una carpeta indicada per l'usuari.
L'usuari indirarà una ruta, el nom de la persona i el seu cognom .
Fes una copia del fitxer ~/.bashrc a la carpeta ruta/cognom/nom*/

        Scanner scanner=new Scanner(System.in);

        //obtener ruta home y fichero .bashrc
        String homePath = System.getProperty("user.home");
        Path path = Path.of(homePath,".bashrc");

        //ruta donde guardarlo, nombre y apellidos
        System.out.println("ruta:");
        String dirUser= scanner.nextLine();
        System.out.println("\nnom:");
        String nom= scanner.nextLine();
        System.out.println("\ncognom:");
        String cognom= scanner.nextLine();

        //convertir string anterior en ruta, crear y copiar dentro .bashrc
        Path ruta = Path.of(dirUser,cognom,nom);
        Files.createDirectories(ruta);
        Path desti = ruta.resolve(".bashrc");
        Files.copy(path, desti);

        //imprimir
        System.out.printf("\nFitxer copiat a la carpeta %s\n", ruta);

    }
}
