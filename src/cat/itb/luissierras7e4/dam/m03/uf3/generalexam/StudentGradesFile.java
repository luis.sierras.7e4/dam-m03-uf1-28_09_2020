package cat.itb.luissierras7e4.dam.m03.uf3.generalexam;


import cat.itb.luissierras7e4.dam.m03.uf2.dataclasses.GradeCalculator;


import java.io.IOException;
import java.nio.file.Path;
import java.util.Scanner;


public class StudentGradesFile {
    public static void main(String[] args) throws IOException {

/*Volem fer un petit programa que llegeixi les notes parcials de diferents estudiants que tenim guardats en un txt
i ens imprimeix la nota dels estudiants i un recompte d'aprovats i suspesos.
Es considerarà suspesos notes inferiors a 5 .

El fitxer contindrà primer el número d'estudiants a llegir.
Després el nom de l'estudiant (és una paraula sense espais) i les notes del exercicis, exàmen i projecte (en aquest ordre). */

        Scanner scanner=new Scanner(System.in);
        String rutaString = scanner.nextLine();
        Path ruta = Path.of(rutaString);

        readText(ruta);


    }

    private static void readText(Path ruta) throws IOException {
        Scanner scanner = new Scanner(ruta);
        int students = scanner.nextInt();
        for (int i=0; i<students; i++) {
            GradeCalculator.readStudent(scanner);
        }
    }
}
