package cat.itb.luissierras7e4.dam.m03.extraordinaria.uf5;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class VehicleRanking {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Vehicle> vehicles = addVehicles(scanner);
        prints(vehicles);
    }

    private static void prints(List<Vehicle> vehicles) {
        System.out.println();
        ordenatsMarca(vehicles);
        System.out.println();
        etiquetaB(vehicles);
        System.out.println();
        lastBMW(vehicles);
        System.out.println();
        matriculats2017(vehicles);
    }

    private static void matriculats2017(List<Vehicle> vehicles) {
        System.out.println("### Matriculats 2017");
        vehicles.stream().filter(any -> any.anyMa == 2017).forEach(System.out::println);
    }

    private static void lastBMW(List<Vehicle> vehicles) {
        System.out.println("### Últim BWM");
        System.out.println(vehicles.stream().filter(marca -> marca.marca.equals("BMW")).max(Comparator.comparingInt(Vehicle::getAnyMa)));
    }

    private static void etiquetaB(List<Vehicle> vehicles) {
        System.out.println("### Vehicles amb etiqueta B");
        vehicles.stream().filter(combustible -> combustible.combustible.equals("Diesel")).filter(any -> any.anyMa >= 2016).sorted(Comparator.comparing(Vehicle::getAnyMa)).forEach(System.out::println);
    }

    private static void ordenatsMarca(List<Vehicle> vehicles) {
        System.out.println("### Vehicles ordenats per marca");
        vehicles.stream().sorted(Comparator.comparing(Vehicle::getModel)).sorted(Comparator.comparing(Vehicle::getMarca)).forEach(System.out::println);
    }


    private static List<Vehicle> addVehicles(Scanner scanner) {
        List<Vehicle> vehicles = new ArrayList<>();
        int v = scanner.nextInt();
        for (int i = 0; i < v; i++) {
            String matricula= scanner.next();
            String marca = scanner.next();
            String model = scanner.next();
            int any= scanner.nextInt();
            String combustible= scanner.next();

            vehicles.add(new Vehicle(matricula,marca,model,any,combustible));
        }
        return vehicles;
    }

}
