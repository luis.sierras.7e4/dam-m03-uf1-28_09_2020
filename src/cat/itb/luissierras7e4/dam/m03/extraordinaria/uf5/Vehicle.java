package cat.itb.luissierras7e4.dam.m03.extraordinaria.uf5;

public class Vehicle {
    String matricula;
    String marca;
    String model;
    int anyMa;
    String combustible;

    public Vehicle(String matricula, String marca, String model, int anyMa, String combustible) {
        this.matricula = matricula;
        this.marca = marca;
        this.model = model;
        this.anyMa = anyMa;
        this.combustible = combustible;
    }

    public String getMarca() {
        return marca;
    }

    public String getModel() {
        return model;
    }

    public int getAnyMa() {
        return anyMa;
    }

    @Override
    public String toString() {
        return matricula + " - " + marca + " " + model + " ("+ anyMa +", "+ combustible+")" ;
    }
}
