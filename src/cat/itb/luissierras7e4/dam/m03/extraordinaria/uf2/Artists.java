package cat.itb.luissierras7e4.dam.m03.extraordinaria.uf2;

public class Artists {
    String nom;
    String pais;

    public Artists(String nom, String pais) {
        this.nom = nom;
        this.pais = pais;
    }

    @Override
    public String toString() {
        return nom +" "+ "("+pais+")";
    }
}
