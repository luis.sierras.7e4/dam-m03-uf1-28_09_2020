package cat.itb.luissierras7e4.dam.m03.extraordinaria.uf2;

public class Albums {
    Artists artist;
    String name;
    int year;

    public Albums(Artists artist, String name, int year) {
        this.artist = artist;
        this.name = name;
        this.year = year;
    }
}
