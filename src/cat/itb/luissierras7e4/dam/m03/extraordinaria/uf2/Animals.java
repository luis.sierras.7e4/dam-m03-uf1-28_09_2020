package cat.itb.luissierras7e4.dam.m03.extraordinaria.uf2;

public class Animals {
    String codi;
    String nom;
    int apats;
    int quantitat;

    public Animals(String codi, String nom, int apats, int quantitat) {
        this.codi = codi;
        this.nom = nom;
        this.apats = apats;
        this.quantitat = quantitat;
    }

    @Override
    public String toString() {
        return codi+"-"+nom+": "+ apats+ " àpats, "+quantitat+"g/apat, total"+ (quantitat*apats)+"g";
    }
}
