package cat.itb.luissierras7e4.dam.m03.extraordinaria.uf2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FoodFeeder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Animals> animals = addAnimal(scanner);
        printAnimals(animals);

    }

    private static void printAnimals(List<Animals> animals) {
        int total = 0;
        String golos = null;
        int apetit= 0;
        System.out.println("---------- Animals ----------");
        for(Animals a:animals){
            System.out.println(a);
            total=total+(a.apats*a.quantitat);
            if(apetit<(a.apats*a.quantitat)){
                golos=(a.codi+"-"+a.nom);
                apetit=(a.apats*a.quantitat);
            }
        }
        System.out.println("---------- Resum ----------");
        System.out.println(total);
        System.out.println(golos);
    }

    private static List<Animals> addAnimal(Scanner scanner) {
        List <Animals> animals = new ArrayList<>();
        int total= scanner.nextInt();
        for (int i = 0; i < total; i++) {
            String codi= scanner.next();
            String nom= scanner.next();
            int apats= scanner.nextInt();
            int quantitat= scanner.nextInt();

            animals.add (new Animals(codi,nom,apats,quantitat));
        }
        return animals;
    }
}
