package cat.itb.luissierras7e4.dam.m03.extraordinaria.uf2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ArtistsAndAlbums {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Artists> artists = addArtist(scanner);
        List<Albums> albums = addAlbum(scanner, artists);
        prints(albums);

    }

    private static void prints(List<Albums> albums) {
        System.out.println("#### Albums");
        for (Albums alb : albums) {
            System.out.println(alb.artist +" - " + alb.name + " - " + alb.year);
        }
    }

    private static List<Albums> addAlbum(Scanner scanner, List<Artists> artists) {
        List<Albums> albums = new ArrayList<>();
        int alb= scanner.nextInt();
        for (int i = 0; i < alb; i++) {
            int art= scanner.nextInt();
            Artists artist=artists.get(art);
            scanner.nextLine();
            String name= scanner.nextLine();
            int year= scanner.nextInt();

            albums.add(new Albums(artist,name,year));
        }
        return albums;
    }

    private static List<Artists> addArtist(Scanner scanner) {
        List<Artists> artists = new ArrayList<>();
        int art= scanner.nextInt();
        for (int i = 0; i < art; i++) {
            String nom= scanner.next();
            String pais= scanner.next();

            artists.add(new Artists(nom,pais));
        }
        return artists;
    }
}
