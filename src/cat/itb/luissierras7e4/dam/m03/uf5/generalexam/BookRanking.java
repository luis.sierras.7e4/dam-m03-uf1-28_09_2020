package cat.itb.luissierras7e4.dam.m03.uf5.generalexam;


import java.util.*;
import java.util.stream.Stream;


public class BookRanking {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<BookClass> books = new ArrayList<>();

        int count = scanner.nextInt();
        for (int i = 0; i < count; i++) {
            BookClass book = readBooks(scanner, books);
            books.add(book);
            books.sort((v1, v2) -> v2.year-v1.year);
        }

        printTops(books);
    }

    private static void printTops(List<BookClass> books) {
        printPages(books);
        System.out.println();
        printYears(books);
        System.out.println();
        printAvg(books);
        System.out.println();
        printAfter2018(books);
        System.out.println();
        //printTotalPages(books) no se com fer-ho ara mateix
        print100Pages(books);

    }

    private static void print100Pages(List<BookClass> books) {
        System.out.println("### Més de 100 pàgines");
        books.stream().filter(bookClass -> bookClass.getPage() > 100).forEach(System.out::println);
    }

    private static void printAfter2018(List<BookClass> books) {
        System.out.println("### Llibres publicats després del 2018");
        books.stream().filter(bookClass -> bookClass.getYear() > 2018).forEach(System.out::println);
    }

    private static void printAvg(List<BookClass> books) {
        System.out.println("### Mitjana de pàgines");
        double Avg = 0;
        for(BookClass book : books) {
            Avg += book.getPage();
        }
        Avg = (Avg/ books.size());
        System.out.printf("%.2f",Avg);

    }

    private static void printYears(List<BookClass> books) {
        System.out.println("### Llibres per any");
        for(BookClass book : books){
            System.out.println(book);
        }

    }

    private static void printPages(List<BookClass> books) {
        System.out.println("### Llibre amb més pàgines");
        System.out.println( Collections.max(books, Comparator.comparing(BookClass::getPage)));
    }

    private static BookClass readBooks(Scanner scanner, List<BookClass> books) {
        scanner.nextLine();
        String bookName = scanner.nextLine();
        String author = scanner.nextLine();
        String ISBN = scanner.nextLine();
        int page = scanner.nextInt();
        int year = scanner.nextInt();
        return new BookClass(bookName, author, ISBN, page, year);
    }


}
