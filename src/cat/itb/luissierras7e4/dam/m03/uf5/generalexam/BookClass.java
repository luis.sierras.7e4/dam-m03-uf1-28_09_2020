package cat.itb.luissierras7e4.dam.m03.uf5.generalexam;

public class BookClass {
    String bookName;
    String author;
    String ISBN;
    int page;
    int year;

    public BookClass(String bookName, String author, String ISBN, int page, int year) {
        this.bookName = bookName;
        this.author = author;
        this.ISBN = ISBN;
        this.page = page;
        this.year = year;
    }

    public String getBookName() {
        return bookName;
    }

    public String getAuthor() {
        return author;
    }

    public String getISBN() {
        return ISBN;
    }

    public int getPage() {
        return page;
    }

    public int getYear() {
        return year;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    @Override
    public String toString() {
        return bookName + " - " + author + " - " + year ;
    }
}
