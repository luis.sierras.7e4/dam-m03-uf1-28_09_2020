package cat.itb.luissierras7e4.dam.m03.uf5.exercices.tads;

public class Employee {
    String dni;
    String name;
    String surname;
    String address;

    public Employee(String dni, String name, String surname, String address) {
        this.dni = dni;
        this.name = name;
        this.surname = surname;
        this.address = address;
    }

    public String getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return String.format("%s %s - %s, %s", name, surname, dni, address);
    }

}
