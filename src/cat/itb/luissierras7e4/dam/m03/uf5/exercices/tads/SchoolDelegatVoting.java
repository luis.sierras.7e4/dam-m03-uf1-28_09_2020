package cat.itb.luissierras7e4.dam.m03.uf5.exercices.tads;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SchoolDelegatVoting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, Integer> votes = countVotes(scanner);
        printVotes(votes);
    }

    private static void printVotes(Map<String, Integer> votes) {
        for (String existingName: votes.keySet()){
            int vote  = votes.get(existingName);
            System.out.printf("%s: %d%n", existingName, vote);
        }
    }

    private static Map<String, Integer> countVotes(Scanner scanner) {
        Map<String, Integer> votes = new HashMap<>();
        String name = scanner.nextLine();
        while(!name.equals("END")) {
            if (!votes.containsKey(name)) {
                votes.put(name, 1);
            } else {
                int newVotes = votes.get(name) + 1;
                votes.put(name, newVotes);
            }
            name = scanner.nextLine();
        }
        return votes;
    }

}
