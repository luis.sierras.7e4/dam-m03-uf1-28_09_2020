package cat.itb.luissierras7e4.dam.m03.uf5.exercices.func;

import cat.itb.luissierras7e4.dam.m03.uf5.exercices.tads.Employee;
import cat.itb.luissierras7e4.dam.m03.uf5.exercices.tads.EmpoyeeById;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FilterEmpoyeeById {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        List<Employee> employeeList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Employee employee = EmpoyeeById.readEmployee(scanner);
            employeeList.add(employee);
        }

        employeeList.removeIf(FilterEmpoyeeById::endsWithA);
        employeeList.forEach(System.out::println);
    }

    private static boolean endsWithA(Employee employee) {
        char lastLetter = employee.getDni().charAt(employee.getDni().length()-1);
        return lastLetter=='A';
    }

}
