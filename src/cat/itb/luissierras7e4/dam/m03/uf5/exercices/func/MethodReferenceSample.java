package cat.itb.luissierras7e4.dam.m03.uf5.exercices.func;

import cat.itb.luissierras7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class MethodReferenceSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> values = IntegerLists.readIntegerList(scanner);

        values.removeIf(MethodReferenceSample::endsWithThree);
        values.sort(MethodReferenceSample::descendingOrder);
        values.forEach(MethodReferenceSample::print);
    }

    private static void print(Integer integer) {
        System.out.println(integer);
    }

    private static int descendingOrder(Integer i1, Integer i2) {
        return i2-i1;
    }

    private static boolean endsWithThree(Integer integer) {
        return integer%10==3;
    }

}
