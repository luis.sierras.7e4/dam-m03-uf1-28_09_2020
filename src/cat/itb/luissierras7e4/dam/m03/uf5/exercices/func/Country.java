package cat.itb.luissierras7e4.dam.m03.uf5.exercices.func;

import java.util.Locale;

public class Country {
    String name;
    String capital;
    int area;
    int density;

    public Country(String name, String capital, int area, int density) {
        this.name = name;
        this.capital = capital;
        this.area = area;
        this.density = density;
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public int getArea() {
        return area;
    }

    public int getDensity() {
        return density;
    }

    @Override
    public String toString() {
        return name+" "+capital+" "+area+" "+density;
    }

    public void toUpper(Country country) {
        this.name = getName().toUpperCase();
        this.capital = getCapital().toUpperCase();
    }
}
