package cat.itb.luissierras7e4.dam.m03.uf5.exercices.tads;

public class Patient implements Comparable<Patient>{
    int priority;
    String name;

    public Patient(int priority, String name) {
        this.priority = priority;
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Patient patient) {
        return patient.priority-priority;
    }
}
