package cat.itb.luissierras7e4.dam.m03.uf5.exercices.func;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class CountryDataSortByName {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Country> countryList = readCountries(scanner);

        countryList.stream()
                .filter(country -> country.getDensity()>5)
                .sorted(Comparator.comparing(Country::getName))
                .forEach(System.out::println);
    }

    public static List<Country> readCountries(Scanner scanner) {
        int count = scanner.nextInt();
        List<Country> countryList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Country country = readCountry(scanner);
            countryList.add(country);
        }
        return countryList;
    }

    private static Country readCountry(Scanner scanner) {
        String name = scanner.next();
        String capital= scanner.next();
        int area= scanner.nextInt();
        int density= scanner.nextInt();
        return new Country(name, capital, area, density);
    }
}

