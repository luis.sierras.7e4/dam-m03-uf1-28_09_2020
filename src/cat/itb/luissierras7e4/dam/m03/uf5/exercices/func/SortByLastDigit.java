package cat.itb.luissierras7e4.dam.m03.uf5.exercices.func;

import cat.itb.luissierras7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class SortByLastDigit {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> values = IntegerLists.readIntegerList(scanner);

        values.sort(Comparator.comparing(SortByLastDigit::getLastDigit));
        values.forEach(System.out::println);
    }

    private static Integer getLastDigit(Integer i) {
        return i%10;
    }

}
