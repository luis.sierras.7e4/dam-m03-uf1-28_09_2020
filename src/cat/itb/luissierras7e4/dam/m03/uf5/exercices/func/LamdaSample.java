package cat.itb.luissierras7e4.dam.m03.uf5.exercices.func;

import cat.itb.luissierras7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class LamdaSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> values = IntegerLists.readIntegerList(scanner);

        values.removeIf(LamdaSample::endsWithTree);
        values.sort((v1, v2) -> v2-v1);
        values.forEach(System.out::println);

    }

    private static boolean endsWithTree(int integer) {
        return integer%10==3;
    }

}
