package cat.itb.luissierras7e4.dam.m03.uf5.exercices.func;

import java.util.List;
import java.util.Scanner;

import static cat.itb.luissierras7e4.dam.m03.uf5.exercices.func.CountryDataSortByName.readCountries;

public class CoubtryDataSortAveragePopulation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Country> countryList = readCountries(scanner);

        double average = countryList.stream()
                .filter(country -> country.getArea() < 1200000)
                .mapToInt(country -> country.getDensity())
                .average().getAsDouble();
        System.out.println("població mitjana: " + average);

    }
}

