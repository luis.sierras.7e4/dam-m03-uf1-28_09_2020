package cat.itb.luissierras7e4.dam.m03.uf5.exercices.func;

import cat.itb.luissierras7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class MetersToCentimeters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> values = IntegerLists.readIntegerList(scanner);
        values.stream().map(integer -> integer*100)
                .forEach(System.out::println);
    }
}
