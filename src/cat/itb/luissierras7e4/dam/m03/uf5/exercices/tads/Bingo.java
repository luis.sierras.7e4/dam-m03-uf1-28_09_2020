package cat.itb.luissierras7e4.dam.m03.uf5.exercices.tads;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Bingo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Set<Integer> numbers = new HashSet<>();

        for (int i = 0; i < 10; i++) {
            int number = scanner.nextInt();
            numbers.add(number);
        }

        while(!numbers.isEmpty()){
            int number = scanner.nextInt();
            numbers.remove(number);
        }
        System.out.println("BINGO");

    }

}
