package cat.itb.luissierras7e4.dam.m03.uf5.exercices.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadSubjectInfo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String regexCurs = "([A-Z]+)[-]M(\\d+)UF(\\d+)";
        Pattern pattern = Pattern.compile(regexCurs);

        int ufs = scanner.nextInt();
        for (int i = 0; i < ufs; i++) {
            String nomUF = scanner.nextLine();
            Matcher matcher = pattern.matcher(nomUF);
        }

        //System.out.printf("Estàs cursant la unitat formativa 2, del mòdul 3 de DAM.");
        //dam-m03uf2
    }
}
