package cat.itb.luissierras7e4.dam.m03.uf5.practica;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

public class CovidApp {
    public static void main(String[] args) throws IOException {
        List<Data> dades = new ArrayList<>();

        Path path = Path.of("src/cat/itb/luissierras7e4/dam/m03/uf5/practica/coviddata.txt");
        Scanner scanner = new Scanner(path);

        while (scanner.hasNextLine()) {
            String nameCountry = (scanner.nextLine());
            String codeCountry = (scanner.nextLine());
            int newCases = (scanner.nextInt());
            int totalCases = (scanner.nextInt());
            int newDeaths = (scanner.nextInt());
            int totalDeaths = (scanner.nextInt());
            int newRecovered = (scanner.nextInt());
            int totalRecovered = (scanner.nextInt());

            dades.add(new Data(nameCountry, codeCountry, newCases, totalCases,
                    newDeaths, totalDeaths, newRecovered, totalRecovered));
            scanner.nextLine();
        }
        printResults(dades);

    }

    private static void printResults(List<Data> dades) throws IOException {

        System.out.println();
        Totals.printTotal(dades);

        System.out.println();
        Tops.printTop(dades);

        System.out.println();
        Tops.printTopEU(dades);

        System.out.println();
        Data ESPANYA = Population.getESPANYA(dades);
        Population.printSpainByPopulation(ESPANYA);

    }


}

