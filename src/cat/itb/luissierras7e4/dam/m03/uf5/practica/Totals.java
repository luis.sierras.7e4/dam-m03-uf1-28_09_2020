package cat.itb.luissierras7e4.dam.m03.uf5.practica;

import java.util.List;

public class Totals {
    // Totals
    static void printTotal(List<Data> dades) {
        System.out.printf("### DADES TOTALS ###\n" +
                        "Casos nous: %s\n" +
                        "Casos totals: %s\n" +
                        "Noves morts: %s\n" +
                        "Morts totals: %s\n" +
                        "Nous recuperats: %s\n" +
                        "Recuperats totals: %s\n", countNewCases(dades), countTotalCases(dades), countNewDeaths(dades)
                , countTotalsDeaths(dades), countNewRecovered(dades), countTotalsRecovered(dades));
    }

    private static Object countNewCases(List<Data> dades) {
        int total = 0;
        for (Data dada : dades) {
            int newCases = dada.getNewCases();
            total += newCases;
        }
        return total;
    }

    private static Object countTotalCases(List<Data> dades) {
        int total = 0;
        for (Data dada : dades) {
            int totalCases = dada.getTotalCases();
            total += totalCases;
        }
        return total;
    }

    private static Object countNewDeaths(List<Data> dades) {
        int total = 0;
        for (Data dada : dades) {
            int newCases = dada.getNewDeaths();
            total += newCases;
        }
        return total;
    }

    private static Object countTotalsDeaths(List<Data> dades) {
        int total = 0;
        for (Data dada : dades) {
            int newCases = dada.getTotalDeaths();
            total += newCases;
        }
        return total;
    }

    private static Object countNewRecovered(List<Data> dades) {
        int total = 0;
        for (Data dada : dades) {
            int newCases = dada.getNewRecovered();
            total += newCases;
        }
        return total;
    }

    private static Object countTotalsRecovered(List<Data> dades) {
        int total = 0;
        for (Data dada : dades) {
            int newCases = dada.getTotalRecovered();
            total += newCases;
        }
        return total;
    }
}
