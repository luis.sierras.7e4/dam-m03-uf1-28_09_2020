package cat.itb.luissierras7e4.dam.m03.uf5.practica;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Population {

    static Data getESPANYA(List<Data> dades) {
        for (Data dada : dades) {
            if (dada.getCodeCountry().equals("ES")) {
                return dada;
            }
        }
        return null;
    }

    static void printSpainByPopulation(Data dada) throws IOException {
        System.out.printf("### Spain Relative ###\n" +
                        "Casos nous relatius: %.2f%%\n" +
                        "Casos totals relatius: %.2f%%\n" +
                        "Noves morts relatius: %.2f%%\n" +
                        "Morts totals relatius: %.2f%%\n" +
                        "Nous recuperats relatius: %.2f%%\n" +
                        "Recuperats totals relatius: %.2f%%\n",
                relativeNewCases(dada), relativeTotalCases(dada), relativeNewDeaths(dada),
                relativeTotalsDeaths(dada), relativeNewRecovered(dada), relativeTotalRecovered(dada));
    }

    private static String getCodeThreeD(String id) throws IOException {
        Path path = Paths.get("src/cat/itb/luissierras7e4/dam/m03/uf5/practica/country_codes.txt");
        Scanner scanner = new Scanner(path);
        while (scanner.hasNext()) {
            if (scanner.next().equals(id)) {
                return scanner.next();
            }
        }
        return null;
    }

    private static int getPopulation(String id) throws IOException {
        Path path = Paths.get("src/cat/itb/luissierras7e4/dam/m03/uf5/practica/population.txt");
        Scanner scanner = new Scanner(path);
        while (scanner.hasNext()) {
            if (scanner.next().equals(id)) {
                return scanner.nextInt();
            }
        }
        return 0;
    }

    private static double relativeNewCases(Data dada) throws IOException {
        String ID = getCodeThreeD(dada.getCodeCountry());
        int population = getPopulation(ID);
        return ((double) dada.getNewCases() / population) * 100;
    }

    private static double relativeTotalCases(Data dada) throws IOException {
        String ID = getCodeThreeD(dada.getCodeCountry());
        int population = getPopulation(ID);
        return ((double) dada.getTotalCases() / population) * 100;
    }

    private static double relativeNewDeaths(Data dada) throws IOException {
        String ID = getCodeThreeD(dada.getCodeCountry());
        int population = getPopulation(ID);
        return ((double) dada.getNewDeaths() / population) * 100;
    }

    private static double relativeTotalsDeaths(Data dada) throws IOException {
        String ID = getCodeThreeD(dada.getCodeCountry());
        int population = getPopulation(ID);
        return ((double) dada.getTotalDeaths() / population) * 100;
    }

    private static double relativeNewRecovered(Data dada) throws IOException {
        String ID = getCodeThreeD(dada.getCodeCountry());
        int population = getPopulation(ID);
        return ((double) dada.getNewRecovered() / population) * 100;
    }

    private static double relativeTotalRecovered(Data dada) throws IOException {
        String ID = getCodeThreeD(dada.getCodeCountry());
        int population = getPopulation(ID);
        return ((double) dada.getTotalRecovered() / population) * 100;
    }

}
