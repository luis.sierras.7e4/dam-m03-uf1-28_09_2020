package cat.itb.luissierras7e4.dam.m03.uf2.classfun;

import cat.itb.luissierras7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.sql.ClientInfoStatus;
import java.util.List;
import java.util.Scanner;

public class PeopleCounter {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        List<Integer> people = IntegerLists.readIntegerList(scanner);
        System.out.print(IntegerLists.sum(people));
    }
}
