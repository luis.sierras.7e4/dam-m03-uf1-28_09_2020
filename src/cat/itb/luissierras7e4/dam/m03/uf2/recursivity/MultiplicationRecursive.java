package cat.itb.luissierras7e4.dam.m03.uf2.recursivity;

import java.util.Scanner;

public class MultiplicationRecursive {
    public static int multiply(int n, int m){
        if(n==0 || m==0){
            return 0;
        }else if(m==1){
            return n;
        }else if(n==1){
            return m;
        }else{
            return n+multiply(n,m-1);
        }
    }

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        int m= scanner.nextInt();
        System.out.println(multiply(n,m));
    }
}
