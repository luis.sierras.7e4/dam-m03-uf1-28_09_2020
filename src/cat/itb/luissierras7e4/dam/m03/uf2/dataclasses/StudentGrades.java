package cat.itb.luissierras7e4.dam.m03.uf2.dataclasses;

public class StudentGrades {
    String name;
    double notaExercicis;
    double notaExamen;
    double notaProjecte;

    public StudentGrades(String name, double notaExercicis, double notaExamen, double notaProjecte) {
        this.name = name;
        this.notaExercicis = notaExercicis;
        this.notaExamen = notaExamen;
        this.notaProjecte = notaProjecte;
    }

    public double getAverageGrade(){
        return notaExercicis*0.3+ notaExamen*0.3+notaProjecte*0.4;
    }

    public String getName() {
        return name;
    }

    public double getNotaExercicis() {
        return notaExercicis;
    }

    public double getNotaExamen() {
        return notaExamen;
    }

    public double getNotaProjecte() {
        return notaProjecte;
    }

    public String toString(){
        return "Student: "+getName();
    }
}
