package cat.itb.luissierras7e4.dam.m03.uf2.dataclasses;

import java.util.Scanner;

public class Product {
    String name;
    Double price;

    public Product(String name, Double price){
        this.name=name;
        this.price=price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public static void printProduct(Product product) {
        String name =product.getName();
        double price = product.getPrice();

        System.out.printf("El producte %s val %.2f€%n", name, price);

    }

    public static Product readProduct(Scanner scanner) {
        String name= scanner.next();
        double price= scanner.nextDouble();
        return new Product(name, price);
    }
}
