package cat.itb.luissierras7e4.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MultiProductInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Product> products = readProducts(scanner);
        printProducts(products);
    }

    private static void printProducts(List<Product> products) {
        for(Product p : products){
            Product.printProduct(p);
        }
    }

    private static List<Product> readProducts(Scanner scanner) {
        int productCount = scanner.nextInt();
        scanner.nextLine();
        List<Product> products = new ArrayList<>();
        for(int i=0; i<productCount; ++i){
            Product product = Product.readProduct(scanner);
            products.add(product);
        }
        return products;
    }
}
