package cat.itb.luissierras7e4.dam.m03.uf2.dataclasses;

import java.util.Scanner;

public class Student {
    School school;
    String fullName;

    public Student(School school, String fullName){
        this.school = school;
        this.fullName = fullName;
    }

    public School getSchool() {
        return school;
    }

    public String getFullName() {
        return fullName;
    }

    public static Student readStudentInfo(Scanner scanner, School school){
        scanner.nextLine();
        String fullName = scanner.nextLine();
        return new Student(school, fullName);
    }

    public static void printStudentInfo(Student student){
        School.printSchool(student.getSchool());
        System.out.printf("%n%s%n--------------------", student.getFullName());
    }
}

