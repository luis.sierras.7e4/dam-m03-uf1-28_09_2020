package cat.itb.luissierras7e4.dam.m03.uf2.dataclasses;

import java.util.Scanner;

public class SchoolInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        School school = School.readSchool(scanner);
        School.printSchool(school);
    }
}

