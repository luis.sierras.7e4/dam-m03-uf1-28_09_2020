package cat.itb.luissierras7e4.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StudentInfoPrinter {
    public static void main(String[] args) {
        List<School> schools = readSchoolsList();
        List<Student> students = readStudentsList(schools);

        for (Student student : students){
            Student.printStudentInfo(student);
        }

    }

    private static List<Student> readStudentsList(List<School> schools) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Número d'estudiants a llegir:");
        int size = scanner.nextInt();
        List<Student> students = new ArrayList<Student>();
        for (int i = 0; i < size; ++i){
            int schoolNum = scanner.nextInt();
            Student student = Student.readStudentInfo(scanner, schools.get(schoolNum));
            students.add(student);
        }
        return students;
    }

    private static List<School> readSchoolsList() {
        System.out.println("Número d'escoles a llegir:");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        List<School> schools = new ArrayList<>();
        for (int i = 0; i < size; ++i){
            scanner.nextLine();
            School school = School.readSchool(scanner);
            schools.add(school);

        }
        return schools;
    }
}
