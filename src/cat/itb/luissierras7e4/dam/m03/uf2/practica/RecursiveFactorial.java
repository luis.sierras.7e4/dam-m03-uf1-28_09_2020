package cat.itb.luissierras7e4.dam.m03.uf2.practica;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RecursiveFactorial {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> values = new ArrayList<>();
        int size = scanner.nextInt();
        for (int i=0; i<size; i++){
            int value = scanner.nextInt();
            values.add(value);
        }

        for (int value : values){
            int result = factorialValue(value);
            System.out.println(result);
        }
    }

    /**
     * Calculates the factorial from a number
     * @param value user number
     * @return factorial from the value
     */
    private static int factorialValue(int value) {
        if(value == 0)
            return 1;
        else{
            return value * factorialValue(value - 1);
        }
    }
}