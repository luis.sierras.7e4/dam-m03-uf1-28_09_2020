package cat.itb.luissierras7e4.dam.m03.uf2.practica;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Peaks {
    String name;
    int height;
    String country;
    int distance;
    int time;

    public Peaks(String name, int height, String country, int distance, int time) {
        this.name = name;
        this.height = height;
        this.country = country;
        this.distance = distance;
        this.time = time;

    }

    public String getName() { return name; }

    public int getHeight() { return height; }

    public String getCountry() { return country; }

    public int getDistance() { return distance; }

    public int getTime() { return time; }

    public int getHours() { return time / 60; }

    public int getMinutes() { return time - (getHours() * 60); }

    public double getSpeed() {
        return (double)distance / time * 60 / 1000;
    }

    @Override
    public String toString() {
        return String.format("%s - %s (%dm) - Temps: %02d:%02d"
                , name, country, height, getHours(), getMinutes());
    }
    
    /**
     * Reads a new peak
     * @param scanner data
     * @return new Peaks object
     */
    public static Peaks readPeack(Scanner scanner) {
        scanner.nextLine();
        String name = scanner.nextLine();
        int height = scanner.nextInt();
        scanner.nextLine();
        String country = scanner.nextLine();
        int distance = scanner.nextInt();
        int time = scanner.nextInt();
        return new Peaks(name, height, country, distance, time);
    }

    /**
     * Given a list of peaks, returns the highest peak
     * @param peaks list of Peaks
     */
    public static void getHigherPeak(List<Peaks> peaks) {
        int i = 1;
        Peaks higherPeak = peaks.get(0);
        while (i < peaks.size()) {
            if (peaks.get(i).getHeight() > higherPeak.getHeight())
                higherPeak = peaks.get(i);
            i++;
        }
        System.out.printf("Cim més alt: %s (%dm)%n",
                higherPeak.getName(), higherPeak.getHeight());
    }

    /**
     * Given a list of peaks, returns the fastest scalable peak (less time)
     * @param peaks list of Peaks
     */
    public static void getLowerTimePeak(List<Peaks> peaks) {
        int i = 1;
        Peaks lowerTimePeak = peaks.get(0);
        while (i < peaks.size()) {
            if (peaks.get(i).getTime() < lowerTimePeak.getTime())
                lowerTimePeak = peaks.get(i);
            i++;
        }
        System.out.printf("Cim és ràpid: %s (%02d:%02d)%n"
                , lowerTimePeak.getName(), lowerTimePeak.getHours(), lowerTimePeak.getMinutes());
    }

    /**
     * Given a list of peaks, returns the climbing speed of the peak
     * @param peaks list of Peaks
     */
    public static void getFasterPeak(List<Peaks> peaks) {
        List<Double> speeds = new ArrayList<>();
        for (Peaks peak : peaks) {
            double speed = peak.getSpeed();
            speeds.add(speed);
        }
        int i = 1;
        Peaks fasterPeak = peaks.get(0);
        double faster = speeds.get(0);
        while (i < speeds.size()) {
            if (speeds.get(i) > faster)
                fasterPeak = peaks.get(i);
            i++;
        }
        System.out.printf("Cim més veloç: %s %.2fkm/hora%n"
                , fasterPeak.getName(), fasterPeak.getSpeed());
    }
}