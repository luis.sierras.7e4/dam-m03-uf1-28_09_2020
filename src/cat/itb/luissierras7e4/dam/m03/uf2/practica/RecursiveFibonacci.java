package cat.itb.luissierras7e4.dam.m03.uf2.practica;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RecursiveFibonacci {
    //Only returns correct results if the userValue is
    //between the 20 first numbers from the sequence
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = 20;
        List<Integer> fibonacci = new ArrayList<>();
        while(number != 0){
            fibonacci.add(fibonacciValue(number));
            number--;
        }

        int userValue = scanner.nextInt();
        boolean isFibonacciNumber = false;
        for (int value : fibonacci){
            if (userValue == value){
                isFibonacciNumber = true;
                break;
            }
        }
        System.out.println(isFibonacciNumber);
    }

    /**
     * Calculates the value in the number position from the Fibonacci sequence
     * @param number position from Fibonacci list
     * @return value in number position
     */
    private static int fibonacciValue(int number) {
        if (number == 0 || number == 1)
            return number;
        else {
            return fibonacciValue(number-1) + fibonacciValue(number-2);
        }
    }
}