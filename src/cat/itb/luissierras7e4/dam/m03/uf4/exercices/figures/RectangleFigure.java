package cat.itb.luissierras7e4.dam.m03.uf4.exercices.figures;

import java.io.PrintStream;

public class RectangleFigure extends Figure{
    int width;

    public RectangleFigure(String color, int width, int height) {
        super(color);
        this.width = width;
        this.height = height;
    }

    int height;

    public void paint(PrintStream printStream){
        printStream.print(color);
        for(int i=0; i<height;i++) {
            for (int j = 0; j < width; j++) {
                printStream.print("x");
            }
            printStream.println();
        }
        printStream.println(ConsoleColors.RESET);
    }
}
