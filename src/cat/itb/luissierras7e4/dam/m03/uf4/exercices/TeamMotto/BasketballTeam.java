package cat.itb.luissierras7e4.dam.m03.uf4.exercices.TeamMotto;

public class BasketballTeam extends Team {

    public BasketballTeam(String nomEquip, String motto) {
        super(nomEquip, motto);
    }

    @Override
    public void shoutMotto(){
        System.out.println("1,2,3 "+motto);
    }
}
