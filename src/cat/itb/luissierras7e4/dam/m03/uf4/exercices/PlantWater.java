package cat.itb.luissierras7e4.dam.m03.uf4.exercices;

import java.util.List;

public interface PlantWater {
    List<Double> getHumidityRecord();
    void startWatterSystem();
}
