package cat.itb.luissierras7e4.dam.m03.uf4.exercices.figures;

import java.io.PrintStream;

public class LeftPiramidFigure extends Figure{

    int base;

    public LeftPiramidFigure(String color, int base) {
        super(color);
        this.base = base;
    }

    public void paint(PrintStream printStream){
        printStream.print(color);
        for(int i=0; i<base;i++) {
            for (int j = 0; j < i+1; j++) {
                printStream.print("x");
            }
            printStream.println();
        }
        printStream.println(ConsoleColors.RESET);
    }

}
