package cat.itb.luissierras7e4.dam.m03.uf4.exercices.TeamMotto;

import java.util.ArrayList;
import java.util.List;

public class TeamMotto {
    public static void main(String[] args) {

        List<Team> teams = new ArrayList<>();

        teams.add(new BasketballTeam("Mosques","Bzzzanyarem"));
        teams.add(new VolleyTeam("Dragons","Grooarg","verd"));
        teams.add(new GolfTeam("Abelles","Piquem Fort","Marta Ahuja"));

        shoutMottos(teams);
    }
    public static void shoutMottos(List<Team> teams){
        for(Team team: teams)
            team.shoutMotto();
    }

}
