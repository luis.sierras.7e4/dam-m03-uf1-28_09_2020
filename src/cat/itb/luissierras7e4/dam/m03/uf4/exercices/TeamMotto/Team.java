package cat.itb.luissierras7e4.dam.m03.uf4.exercices.TeamMotto;

public abstract class Team {
    String nomEquip;
    String motto;


    public Team(String nomEquip, String motto) {
        this.nomEquip = nomEquip;
        this.motto = motto;
    }

    public void shoutMotto() {
        System.out.println(motto);
    }

}
