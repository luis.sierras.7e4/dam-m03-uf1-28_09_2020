package cat.itb.luissierras7e4.dam.m03.uf4.exercices.bicycles;

public class VehicleModel {
    public static void main(String[] args) {
        Marca marca = new Marca("lorem", "Espedi");

        Model modelo1 = new Model("Fast",5, marca);
        ModelPatinet modelo2 = new ModelPatinet("Franchesco",15, marca);

        System.out.println(modelo1);
        System.out.println(modelo2);
    }
}
