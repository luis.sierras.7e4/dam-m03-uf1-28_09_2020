package cat.itb.luissierras7e4.dam.m03.uf4.exercices;

public class StudentWithTextGrade {
    String name;
    Nota nota;

    public enum Nota {
        SUSPES, APROVAT, BE, NOTABLE, EXCELLENT
    }

    public StudentWithTextGrade(String name, Nota nota) {
        this.name = name;
        this.nota = nota;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", nota=" + nota +
                '}';
    }
}
