package cat.itb.luissierras7e4.dam.m03.uf4.exercices.figures;

public class ThreeRectangles {
    public static void main(String[] args) {

/*
color: RED, llargada: 4, amplada: 5
color: YELLOW, llargada: 2, amplada: 2
color: GREEN, llargada: 3, amplada: 5
*/
        RectangleFigure rectangleRED= new RectangleFigure(ConsoleColors.RED,4,5);
        rectangleRED.paint(System.out);
        RectangleFigure rectangleYELLOW= new RectangleFigure(ConsoleColors.YELLOW,2,2);
        rectangleYELLOW.paint(System.out);
        RectangleFigure rectangleGREEN= new RectangleFigure(ConsoleColors.GREEN,3,5);
        rectangleGREEN.paint(System.out);

    }
}
