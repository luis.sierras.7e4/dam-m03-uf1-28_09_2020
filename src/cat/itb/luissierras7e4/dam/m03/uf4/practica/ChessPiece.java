package cat.itb.luissierras7e4.dam.m03.uf4.practica;

import cat.itb.luissierras7e4.dam.m03.uf4.exercices.figures.ConsoleColors;

public abstract class ChessPiece {
    boolean white;

    public ChessPiece(boolean white) {
        this.white = white;
    }

    public void paint(){
        if (white){
            System.out.print(ConsoleColors.YELLOW);
        }else {
            System.out.print(ConsoleColors.BLUE);
        }
    }

    public abstract String get_PieceString();

    public abstract boolean canMove(int i1, int j1, int i2, int j2);
}
//comentario sin sentido para poder subir con tag :)