package cat.itb.luissierras7e4.dam.m03.uf4.practica;

public class Knight extends ChessPiece {
    String knight = "♞";

    public Knight(boolean white) {
        super(white);
    }

    @Override
    public String get_PieceString() {
        paint();
        return knight;
    }

    @Override
    public boolean canMove(int i1, int j1, int i2, int j2) {
        if ((i1 - i2) * (i1 - i2) + (j1 - j2) * (j1 - j2) == 5) {
            return true;
        } else {
            return false;
        }
    }
}
//comentario sin sentido para poder subir con tag :)