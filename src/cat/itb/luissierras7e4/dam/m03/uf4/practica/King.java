package cat.itb.luissierras7e4.dam.m03.uf4.practica;

public class King extends ChessPiece {
    String king = "♚";

    public King(boolean white) {
        super(white);

    }

    @Override
    public String get_PieceString() {
        paint();
        return king;
    }

    @Override
    public boolean canMove(int i1, int j1, int i2, int j2) {
        if (i1 - i2 <= 1 && j1 - j2 <= 1) {
            return true;
        } else {
            return false;
        }
    }
}
//comentario sin sentido para poder subir con tag :)