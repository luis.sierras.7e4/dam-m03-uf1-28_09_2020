package cat.itb.luissierras7e4.dam.m03.uf4.practica;

import cat.itb.luissierras7e4.dam.m03.uf4.exercices.figures.ConsoleColors;

public class ChessBoard {
    ChessPiece[][] board = new ChessPiece[8][8];

    public ChessBoard() {
        board[0][0] = new Rock(true);
        board[0][1] = new Knight(true);
        board[0][2] = new Bishop(true);
        board[0][3] = new Queen(true);
        board[0][4] = new King(true);
        board[0][5] = new Bishop(true);
        board[0][6] = new Knight(true);
        board[0][7] = new Rock(true);
        for (int i = 0; i < 8; i++) {
            board[1][i] = new Pawn(true);
        }

        for (int i = 0; i < 8; i++) {
            board[6][i] = new Pawn(false);
        }
        board[7][0] = new Rock(false);
        board[7][1] = new Knight(false);
        board[7][2] = new Bishop(false);
        board[7][3] = new Queen(false);
        board[7][4] = new King(false);
        board[7][5] = new Bishop(false);
        board[7][6] = new Knight(false);
        board[7][7] = new Rock(false);
    }

    public void paint() {
        System.out.println("  0  1  2 3  4  5 6  7");
        for (int i = 0; i < board.length; i++) {
            System.out.print(i + "|");
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] != null) {
                    System.out.print(board[i][j].get_PieceString());
                    System.out.print(ConsoleColors.RESET);
                    System.out.print("|");
                } else {
                    System.out.print("シ|");
                }
            }
            System.out.println();
        }
    }

    public void move(int i1, int j1, int i2, int j2) {
        if ((board[i1][j1] != null) && board[i1][j1].canMove(i1, j1, i2, j2)) {

            if (board[i2][j2] == null) {
                board[i2][j2] = board[i1][j1];
                board[i1][j1] = null;
            } else {
                if ((board[i1][j1].white && board[i2][j2].white) || (!board[i1][j1].white && !board[i2][j2].white)) {
                    System.out.println();
                    System.out.println("no se puede mover es del mismo color");
                } else {
                    board[i2][j2] = board[i1][j1];
                    board[i1][j1] = null;
                }
            }

        } else {
            System.out.println();
            System.out.println("no es valido el movimiento");
        }
    }
}
//he intentado hacer que las blancas no se pongan encima de blancas y viceversa pero al haber nulas me sale error al
//intentar mover a un lugar vacio, me gustaria lograrlo pero es tarde

//lo he logrado

//comentario sin sentido para poder subir con tag :)