package cat.itb.luissierras7e4.dam.m03.uf4.practica;

public class Queen extends ChessPiece{
    String queen = "♛";

    public Queen(boolean white) {
        super(white);
    }

    @Override
    public String get_PieceString() {
        paint();
        return queen;
    }

    @Override
    public boolean canMove(int i1, int j1, int i2, int j2) {
        if(((i1==i2) || (j1==j2)) || ((i1-i2 == j1-j2) || (i1+i2 == j1+j2))){
            return true;
        }else{
            return false;
        }
    }
}
//comentario sin sentido para poder subir con tag :)