package cat.itb.luissierras7e4.dam.m03.uf4.practica;

public class Pawn extends ChessPiece {
    String pawn = "♟";

    public Pawn(boolean white) {
        super(white);
    }

    @Override
    public String get_PieceString() {
        paint();
        return pawn;
    }

    @Override
    public boolean canMove(int i1, int j1, int i2, int j2) {
        if (i1 - i2 <= 1 && j1 - j2 <= 1) {
            return true;
        } else {
            return false;
        }

    }
}
//he intentado hacer que cada peon vaya en su direccion pero no lo he logrado

//comentario sin sentido para poder subir con tag :)