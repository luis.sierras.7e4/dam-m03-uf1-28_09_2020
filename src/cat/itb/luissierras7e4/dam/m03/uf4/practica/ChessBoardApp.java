package cat.itb.luissierras7e4.dam.m03.uf4.practica;


import java.util.Scanner;

public class ChessBoardApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ChessBoard board = new ChessBoard();
        board.paint();
        System.out.println();

        int i1 = scanner.nextInt();
        while (i1!=-1){
            int j1 = scanner.nextInt();
            int i2 = scanner.nextInt();
            int j2 = scanner.nextInt();
            board.move(i1,j1,i2,j2);
            board.paint();
            System.out.println();
            i1 = scanner.nextInt();
        }

    }
}
//comentario sin sentido para poder subir con tag :)