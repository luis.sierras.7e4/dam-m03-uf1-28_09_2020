package cat.itb.luissierras7e4.dam.m03.uf4.generalexam.twitter;


public class Twitter {
    public static void main(String[] args) {

        Tweets tweet1 = new Tweets("iamdevloper", "07 de gener",
                "Remember, a few hours of trial and error can save you several minutes of looking at the README");

        Tweets tweet2 = new Tweets("softcatala", "29 de març",
                "Avui mateix, #CommonVoiceCAT segueix creixent \uD83D\uDE80:" +
                        "\n\uD83D\uDDE3️ 856 hores enregistrades" +
                        "\n✅ 725 de validades." +
                        "\nSi encara no has participat, pots fer-ho aquí!");

        Tweets polltweet3 = new PollTweets("iamdevloper", "07 de gener",
                "Remember, a few hours of trial and error can save you several minutes of looking at the README",
                "Comèdia dramàtica - La Fúmiga","In the night - Oques Grasses",
                "Una Lluna a l'Aigua - Txarango","Esbarzers - La Gossa Sorda");


        Tweets tweet4 = new Tweets("ProgrammerJokes", "05 d'abril",
                "Q: what's the object-oriented way to become weathy?\n\nA: Inheritance");

        tweet1.printTweet();
        tweet2.printTweet();
        polltweet3.printTweet();
        //((PollTweets) polltweet3).vote()
        //NO LOGRO HACER LAS VOTACIONES
        tweet4.printTweet();
    }
}
