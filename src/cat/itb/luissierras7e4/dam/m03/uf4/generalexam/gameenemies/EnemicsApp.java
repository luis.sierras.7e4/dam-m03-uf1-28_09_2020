package cat.itb.luissierras7e4.dam.m03.uf4.generalexam.gameenemies;


import java.util.ArrayList;
import java.util.List;

public class EnemicsApp {
    public static void main(String[] args) {
        List<Enemics> enemicsList = new ArrayList<>();

        enemicsList.add(new Zombies("Zog",10,"AARRRrrgg"));
        enemicsList.add(new Zombies("Lili",30,"GRAaaArg"));
        enemicsList.add(new Troll("Jum",30,5));
        enemicsList.add(new Goblin("Tim",60));


        enemicsList.get(0).attack(5);


    }
}
