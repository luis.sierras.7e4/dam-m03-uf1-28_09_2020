package cat.itb.luissierras7e4.dam.m03.uf4.generalexam.gameenemies;

public class Troll extends Enemics{
    int resistencia;

    public Troll(String name, int vida, int resistencia) {
        super(name, vida);
        this.resistencia = resistencia;
    }

    @Override
    public int attack(int force) {
        if (vida>0){
            force = resistencia-force;
            vida = vida - force;
        }
        return vida;
    }
}
