package cat.itb.luissierras7e4.dam.m03.uf6.generalexam;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class BooksApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Database database = new Database();
        try (Connection connection = database.connect()) {
            BooksDAO booksDAO = new BooksDAO(database);
            MainMenu menu = new MainMenu(scanner, booksDAO);
            menu.draw();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
