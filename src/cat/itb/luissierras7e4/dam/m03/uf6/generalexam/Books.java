package cat.itb.luissierras7e4.dam.m03.uf6.generalexam;

public class Books {
    String titol;
    String autor;
    String ISBN;
    int numPag;
    int any;

    public Books(String titol, String autor, String ISBN, int any, int numPag) {
        this.titol = titol;
        this.autor = autor;
        this.ISBN = ISBN;
        this.numPag = numPag;
        this.any = any;
    }

    public String getTitol() {
        return titol;
    }

    public String getAutor() {
        return autor;
    }

    public String getISBN() {
        return ISBN;
    }

    public int getNumPag() {
        return numPag;
    }

    public int getAny() {
        return any;
    }

    @Override
    public String toString() {

        return String.format("%s - %s - %s ", getTitol(), getAutor(), getAny());
    }
}
//comentario para hacer commit y añadir tag